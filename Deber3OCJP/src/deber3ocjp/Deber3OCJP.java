/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deber3ocjp;

/**
 *
 * @author julio_000
 */
public class Deber3OCJP {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //tipos de datos primitivos
        char caracter='a';
        int entero=3;
        double doble=4.5;
        long largo=10000;
        float flotante=-1.4f;
        boolean verdadero = true;
        
        //Operadores Unarios
        
        int x=5;
        int y=x++; 
        int z=++x;
        System.out.println(x);
        System.out.println(y);
        System.out.println(z);
        
        //Operadores Binarios
        int a=3,b=4,c=6,d=5;
        int suma=a+b;
        int resta=c-a;
        int multipl=b*c;
        int division=c/a;
        System.out.println(suma);
        System.out.println(resta);
        System.out.println(multipl);
        System.out.println(division);
        
        if(c>a){
            System.out.println(c+" es mayot que "+a);
        }
        if(a<b){
            System.out.println(a+" es menor que "+b);
        }
       
        
        //Conversion de Rpmitivas
        int ent;
        double dos;
        ent=10;
        dos=ent;
        System.out.println(dos);
        
        //Conversion de Primitivas con Metodos
        float rads;
        double dourads;
        rads=2.3456f;
        dourads=Math.cos(rads);
        System.out.println(dourads);
        
        //Casting de Primitivas 
        int siete=7;
        double casti=(double)siete; //el casting es correcto pero no necesario
        
        System.out.println(casti);
        
        //Conversion de objetos
        Tigre tigresito=new Tigre("siberiano", 45, "marron", 7);
        Mamifero mamiferito=tigresito;

        System.out.println(mamiferito.toString());
        
        //Casting de Objetos
        Tigre tigresito2=new Tigre("africano", 50, "orange", 6);
        Mamifero mamiferito2=(Mamifero)tigresito2;
        
        System.out.println(mamiferito2.toString());
    }
    
}
