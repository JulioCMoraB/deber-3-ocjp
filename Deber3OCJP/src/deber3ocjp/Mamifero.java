/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deber3ocjp;

/**
 *
 * @author julio_000
 */
public class Mamifero {
    public String tipo;
    public double peso;
    public String ColorPiel;
    public int edad;

    public Mamifero() {
    }

    public Mamifero(String tipo, double peso, String ColorPiel, int edad) {
        this.tipo = tipo;
        this.peso = peso;
        this.ColorPiel = ColorPiel;
        this.edad = edad;
    }

    @Override
    public String toString() {
        return "Mamifero{" + "tipo=" + tipo + ", peso=" + peso + ", ColorPiel=" + ColorPiel + ", edad=" + edad + '}';
    }
    
    
}
